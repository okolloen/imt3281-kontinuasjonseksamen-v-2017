/**
 * 
 */
package no.ntnu.imt3281.records;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

/**
 * @author oivindk
 *
 */
public class JSON2HashMapTest {
	String JSONData;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		URL url = new URL ("http://musicbrainz.org/ws/2/release/bc2be498-ff42-4a4f-b4df-53d6e3bee74a?inc=artist-credits+labels+discids+recordings&fmt=json");
    	URLConnection connection = url.openConnection();				// Need a bit extra
    	// Must set user-agent, the java default user agent is denied
    	connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
    	// Must set accept to application/json, if not html is returned
    	connection.setRequestProperty("Accept", "application/json");
    	connection.connect();

    	// Read the response
    	StringBuffer sb = new StringBuffer();
    	String tmp;
    	BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    	while ((tmp = br.readLine())!=null) {
    		sb.append(tmp);
    		sb.append("\n\r");
    	}
    	JSONData = sb.toString();
	}

	/**
	 * JSON2HashMap.convert(jsonData) takes a string with json encoded data and converts it to a hashmap.
	 * The key is the full path of keys leading up to the value. For arrays the key is an auto generated index.
	 * 
	 * Take a look at the above url with a JSON formatter (like https://jsonformatter.curiousconcept.com/) and
	 * compare it to the desired results below.
	 * 
	 * The code in demo/ParseJSON.java can be used as a starting point.
	 */
	@Test
	public void testJSON2HashMap() {
		HashMap<String, String> result = JSON2HashMap.convert(JSONData);
		/*
		 // Code below will dump contents of HashMap "result", use it for debuging
		 
		result.entrySet().stream().forEach((entry)-> {
			System.out.println (entry.getKey()+"->"+entry.getValue());
		});
		 */
		assertEquals("A Saucerful of Secrets", result.get("title"));						// Title of the album
		assertEquals("Let There Be More Light", result.get("media.0.tracks.0.title"));		// Title of track 1
		assertEquals("A1", result.get("media.0.tracks.0.number"));							// Track number of track 1 (Side A, track 1)
		assertEquals("12\" Vinyl", result.get("media.0.format"));							// Format of this record
		assertEquals("Pink Floyd", result.get("media.0.tracks.6.recording.artist-credit.0.artist.name")); // Artist performing on track 7
	}
}
