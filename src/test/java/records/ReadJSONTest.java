package no.ntnu.imt3281.records;

import static org.junit.Assert.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

public class ReadJSONTest {

	/**
	 * ReadJSON.read(address) should read the content from address and return it as a string. The address given should contain
	 * JSON data so check that it starts with { and ends with } and is at least 2000 characters long. 
	 */
	@Test
	public void testReadJSON() {
		String result = ReadJSON.read("http://musicbrainz.org/ws/2/release/bc2be498-ff42-4a4f-b4df-53d6e3bee74a?inc=artist-credits+labels+discids+recordings&fmt=json");
		System.out.println(result);
		assertTrue(result.startsWith("{"));
		assertTrue(result.trim().endsWith("}"));	// Ignore trailing linebreak
		assertTrue(result.length()>2000);			// Length might change, but at least 2000 characters
		JSONParser parser = new JSONParser();
		try {
			JSONObject obj = (JSONObject) parser.parse(result);	// Try parsing the result.
		} catch (ParseException e) {
			fail(e.getMessage()); 					// Something is wrong
		}
	}
}
