package demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDB {
	private static final String URL = "jdbc:derby:recordCollection";
	private static Connection con;
	
	public static void main(String[] args) {
		try {
            con = DriverManager.getConnection(URL);
		} catch (SQLException sqle) {		// No database exists
			try {							// Try creating database
				con = DriverManager.getConnection(URL+";create=true");
				setupDB();
			} catch (SQLException sqle1) {	// Unable to create database, exit server
				System.err.println("Unable to create database"+sqle1.getMessage());
				System.exit(-1);
			}
		}
	}

	private static void setupDB() throws SQLException {
		Statement stmt = con.createStatement();
		stmt.execute("CREATE TABLE record (id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
                + "title varchar(255) NOT NULL, "
                + "artist varchar(255) NOT NULL, "
                + "media varchar(32), "
                + "country varchar(16), "
                + "releaseDate varchar(32), "
                + "cover blob(1M), "
                + "PRIMARY KEY  (id))");
		stmt.execute("CREATE TABLE track (id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "
				+ "recordId INTEGER NOT NULL, "
				+ "trackNr varchar(16) NOT NULL,"
				+ "title varchar(255) NOT NULL,"
				+ "artist varchar(255) NOT NULL,"
				+ "length varchar(64) NOT NULL,"
				+ "lyrics long varchar,"
				+ "PRIMARY KEY (id))");
	}
}